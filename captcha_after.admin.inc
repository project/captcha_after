<?php

/**
 * @file
 * Admin part of CAPTCHA After module.
 */

/**
 * CAPTCHA after settings form.
 */
function captcha_after_settings() {

  $form['captcha_after_thresholds'] = array(
    '#type' => 'fieldset',
    '#title' => t('Thresholds'),
    '#description' => t('If <strong>ANY</strong> field below is set to 0, this module will be bypassed and the CAPTCHA will always display.'),
    '#collapsible' => FALSE,
  );

  $form['captcha_after_thresholds']['captcha_after_submit_threshold'] = array(
    '#type' => 'textfield',
    '#title' => 'CAPTCHA After submit threshold',
    '#description' => t('Number of times a user (based on Session ID) is permitted to submit non-valid data into the form before starting to protect form with CAPTCHA. Enter 0 to disable CAPTCHA After functionality.'),
    '#default_value' => variable_get('captcha_after_submit_threshold', 3),
    '#required' => TRUE,
  );

  $form['captcha_after_thresholds']['captcha_after_flooding_threshold'] = array(
    '#type' => 'textfield',
    '#title' => 'CAPTCHA flooding threshold',
    '#description' => t('Number of times a visitor (based on hostname/IP) is allowed to submit a protected form in an hour before starting to protect form with CAPTCHA. This is useful for protecting against repeated (but valid) submissions. Enter 0 to disable this behaviour.'),
    '#default_value' => variable_get('captcha_after_flooding_threshold', 3),
    '#required' => TRUE,
  );

  $form['captcha_after_thresholds']['captcha_after_global_flooding_threshold'] = array(
    '#type' => 'textfield',
    '#title' => 'CAPTCHA global flooding threshold',
    '#description' => t('Number of times <strong>ALL</strong> visitors are allowed to submit a protected form within an hour before starting to protect form with CAPTCHA. This is useful for protecting against flooding from multiple IPs. Enter 0 to disable this behaviour.'),
    '#default_value' => variable_get('captcha_after_global_flooding_threshold', 1000),
    '#required' => TRUE,
  );

  $captcha_forms = captcha_after_get_captcha_forms();

  $form['captcha_after_forms'] = array(
    '#type' => 'fieldset',
    '#title' => t('Forms'),
    '#description' => !empty($captcha_forms) ? t('Enable CAPTCHA After for following CAPTCHA protected forms.') : t('Configure CAPTCHA to protect at least one form in order to enable CAPTCHA After.'),
    '#collapsible' => FALSE,
  );

  $form['captcha_after_forms']['captcha_after_forms'] = array(
    '#type' => 'checkboxes',
    '#options' => $captcha_forms,
    '#default_value' => variable_get('captcha_after_forms', array()),
  );

  return system_settings_form($form);

}

/**
 * Returns array of all forms that are protected by CAPTCHA module.
 *
 * @return
 *   Array of forms ids.
 */
function captcha_after_get_captcha_forms() {
  $forms = array();

  $res = db_query('SELECT form_id FROM {captcha_points} WHERE module IS NOT NULL OR type IS NOT NULL ORDER BY form_id');
  while ($form = db_fetch_object($res)) {
    $forms[$form->form_id] = $form->form_id;
  }

  return $forms;
}
