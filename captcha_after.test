<?php

/**
 * @file
 * Captcha After tests.
 */

/**
 * Basic tests for the CAPTCHA After module.
 */
class CaptchaAfterTestCase extends DrupalWebTestCase {

  /**
   * Test users.
   */
  protected $test_user, $test_user1;

  /**
   * getInfo() returns properties that are displayed in the test selection form.
   */
  public static function getInfo() {
    return array(
      'name' => 'CAPTCHA After',
      'description' => 'Testing of the basic CAPTCHA After functionality.',
      'group' => 'CAPTCHA',
    );
  }

  /**
   * setUp() performs any pre-requisite tasks that need to happen.
   */
  public function setUp() {
    // Enable any modules required for the test.
    parent::setUp('captcha', 'captcha_after');

    // So we can reuse CAPTCHA_WRONG_RESPONSE_ERROR_MESSAGE constant.
    module_load_include('test', 'captcha');

    // Create our test users.
    $this->test_user = $this->drupalCreateUser(array('create page content'));
    $this->test_user1 = $this->drupalCreateUser(array('create page content'));
  }

  /**
   * Testing of submit treshold for user login form.
   */
  public function testCaptchaAfterSubmitTreshold() {
    // Configure captcha protection for user login form.
    module_load_include('inc', 'captcha');
    captcha_set_form_id_setting('user_login', 'captcha/Test');

    // Configure captcha after.
    variable_set('captcha_after_forms', array('user_login' => 'user_login'));

    // Test disabling of submit treshold.
    variable_set('captcha_after_submit_threshold', 0);
    $this->drupalGet('user');
    $this->assertField('captcha_response');

    // Test enabling of submit treshold.
    variable_set('captcha_after_submit_threshold', 3);
    $this->drupalGet('user');
    $this->assertNoField('captcha_response');

    // Test clean login.
    $edit = array(
      'name' => $this->test_user->name,
      'pass' => $this->test_user->pass_raw,
    );
    $this->drupalPost('user', $edit, t('Log in'));
    $this->assertLink(t('Log out'));
    $this->drupalLogout();

    // Test bad login treshold limit 1.
    $edit['pass'] .= 'wrong pass';
    $this->drupalPost('user', $edit, t('Log in'));
    $this->assertText('Sorry, unrecognized username or password.');
    $this->assertNoField('captcha_response');

    // Test bad login treshold limit 2.
    $this->drupalPost('user', $edit, t('Log in'));
    $this->assertText('Sorry, unrecognized username or password.');
    $this->assertNoField('captcha_response');

    // Test bad login treshold limit 3.
    $this->drupalPost('user', $edit, t('Log in'));
    $this->assertText('Sorry, unrecognized username or password.');
    $this->assertField('captcha_response');

    // Try to login with incorect captcha solution.
    $edit['pass'] = $this->test_user->pass_raw;
    $edit['captcha_response'] = '?';
    $this->drupalPost('user', $edit, t('Log in'));
    $this->assertText(CAPTCHA_WRONG_RESPONSE_ERROR_MESSAGE);
    $this->assertField('captcha_response');

    // Try to login with correct captcha solution.
    $edit['captcha_response'] = 'Test 123';
    $this->drupalPost('user', $edit, t('Log in'));
    $this->assertLink(t('Log out'));
    $this->assertNoField('captcha_response', 'Submit treshold test finished.');
  }

  /**
   * Testing of flooding tresholds for node/add/page form.
   */
  public function testCaptchaAfterFloodingTresholds() {
    // Order of execution is important thats why we are calling manualy this methods.
    $this->doTestCaptchaAfterFloodingTreshold();
    $this->doTestCaptchaAfterGlobalFloodingTreshold();
  }

  /**
   * Testing of flooding treshold for node/add/page form.
   */
  public function doTestCaptchaAfterFloodingTreshold() {
    // Login test user.
    $this->drupalLogin($this->test_user);

    // Test clean page post without captcha.
    $edit = array(
      'title' => $this->randomName(8),
      'body' => $this->randomString(32),
    );
    $this->drupalPost('node/add/page', $edit, t('Save'));
    $this->assertText(t('Page @title has been created', array('@title' => $edit['title'])));

    // Turno on captcha protection.
    module_load_include('inc', 'captcha');
    captcha_set_form_id_setting('page_node_form', 'captcha/Test');

    // Check default captcha protection.
    $this->drupalGet('node/add/page');
    $this->assertField('captcha_response');

    // Turn on captcha after for node/add/page form.
    variable_set('captcha_after_forms', array('page_node_form' => 'page_node_form'));

    // Test skiping of all checks - we should see captcha response in this case.
    variable_set('captcha_after_submit_threshold', 0);
    variable_set('captcha_after_flooding_threshold', 0);
    variable_set('captcha_after_global_flooding_threshold', 0);
    $this->drupalGet('node/add/page');
    $this->assertField('captcha_response');
    
    // Test flooding treshold. We need to set submit and global tresholds also to not equal to 0.
    variable_set('captcha_after_submit_threshold', 3);
    variable_set('captcha_after_flooding_threshold', 2); // Two submits per hour.
    variable_set('captcha_after_global_flooding_threshold', 1000);
    $this->drupalGet('node/add/page');
    $this->assertNoField('captcha_response');
    
    $this->drupalPost('node/add/page', $edit, t('Save'));
    $this->assertText(t('Page @title has been created', array('@title' => $edit['title'])));

    $this->drupalGet('node/add/page');
    $this->assertNoField('captcha_response');

    $this->drupalPost('node/add/page', $edit, t('Save'));
    $this->assertText(t('Page @title has been created', array('@title' => $edit['title'])));

    $this->drupalGet('node/add/page');
    $this->assertField('captcha_response');

    // Try with bad captcha solution.
    $edit['captcha_response'] = '?';
    $this->drupalPost('node/add/page', $edit, t('Save'));
    $this->assertText(CAPTCHA_WRONG_RESPONSE_ERROR_MESSAGE);
    $this->assertField('captcha_response', 'Flooding treshold test finished.');

    // Try with good captcha solution.
    $edit['captcha_response'] = 'Test 123';
    $this->drupalPost('node/add/page', $edit, t('Save'));
    $this->assertText(t('Page @title has been created', array('@title' => $edit['title'])), 'Flooding treshold test finished.');
  }

  /**
   * Testing of global flooding treshold.
   */
  public function doTestCaptchaAfterGlobalFloodingTreshold() {
    // Test global flooding treshold.
    variable_set('captcha_after_submit_threshold', 3);
    variable_set('captcha_after_flooding_threshold', 10); // While testing we are on a same ip so this needs to be bigger then global floding value.
    variable_set('captcha_after_global_flooding_threshold', 5); // Four global submits per hour.

    // Lets test two more submits for other test user.
    $this->drupalLogin($this->test_user1);

    $this->drupalGet('node/add/page');
    $this->assertNoField('captcha_response');

    $edit = array(
      'title' => $this->randomName(8),
      'body' => $this->randomString(32),
    );

    $this->drupalPost('node/add/page', $edit, t('Save'));
    $this->assertText(t('Page @title has been created', array('@title' => $edit['title'])));

    $this->drupalGet('node/add/page');
    $this->assertNoField('captcha_response');

    $this->drupalPost('node/add/page', $edit, t('Save'));
    $this->assertText(t('Page @title has been created', array('@title' => $edit['title'])));
    
    $this->drupalGet('node/add/page');
    $this->assertField('captcha_response');

    // Try with bad captcha solution.
    $edit['captcha_response'] = '?';
    $this->drupalPost('node/add/page', $edit, t('Save'));
    $this->assertText(CAPTCHA_WRONG_RESPONSE_ERROR_MESSAGE);
    $this->assertField('captcha_response', 'Global flooding treshold test finished.');

    // Try with good captcha solution.
    $edit['captcha_response'] = 'Test 123';
    $this->drupalPost('node/add/page', $edit, t('Save'));
    $this->assertText(t('Page @title has been created', array('@title' => $edit['title'])), 'Global flooding treshold test finished.');
  }
  
}
